import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:food/models/models.dart';

part 'user_services.dart';
part 'food_services.dart';
part 'transaction_services.dart';

String baseUrl = 'http://foodmarket-backend.buildwithangga.id/api/';
