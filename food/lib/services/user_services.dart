part of 'services.dart';

class UserServices {
  //2.1 buat static signIn
  //2.1 jadi yg dikirim email dan password
  static Future<ApiReturnValue<User>> signIn(
      String email, String password) async {
    //koneksi nya nanti akan delay sekitar setengah detik
    await Future.delayed(Duration(milliseconds: 500));

    return ApiReturnValue(value: mockUser);
    // return ApiReturnValue(message: 'wrong email or password');
  }

  static Future<ApiReturnValue<User>> signUp(User user, String password,
      {File pictureFile, http.Client client}) async {
    //5.2 kalau client nya null kita akan buat http client yg baru
    if (client == null) {
      client = http.Client();
    }
    String url = baseUrl + 'register';

    var response = await client.post(url,
        headers: {'Content-Type': 'application/json'},
        //5.2 body nya berbentuk jsonEncode
        //yg belum paham kenapa string string ??
        body: jsonEncode(<String, String>{
          //5.2 nah yg distring ini sesuai dgn example request register
          //pengisian name berasal dari user *param diatas dgn name
          //kalau password karena param diatas sudah ada password jdi tidak perlu user
          'name': user.name,
          'email': user.email,
          'password': password,
          'password_confirmation': password,
          'address': user.address,
          'city': user.city,
          'houseNumber': user.houseNumber,
          'phoneNumber': user.phoneNumber
        }));

    //jika gagal akan dikembalikan ke apireturn value dgn messaage
    if (response.statusCode != 200) {
      return ApiReturnValue(message: 'please try again');
    }
    //kalau berhasil maka json nya akan kita decode
    var data = jsonDecode(response.body);

    //yg akan dilakukan selanjutnya adl menyimpan token yg dikembalikan
    User.token =
        data['data']['access_token']; //ini didapat dari example response
    //dan ini adl user yg akan dikembalikan
    User value = data['data']['user'];

    //upload profile picture
    if (pictureFile != null) {
      ApiReturnValue<String> result = await uploadProfilePicture(pictureFile);
      if (result.value != null) {
        value = value.copyWith(
            picturePath:
                'http://foodmarket-backend.buildwithangga.id/storage/' +
                    result.value);
      }
    }

    return ApiReturnValue(value: value);
    //jadi buat sivalue itu untuk mengembalikan apireturnvalue yg berisi value
  }

  static Future<ApiReturnValue<String>> uploadProfilePicture(File pictureFile,
      {http.MultipartRequest request}) async {
    String url = baseUrl + 'user/photo';
    var uri = Uri.parse(url);

    if (request == null) {
      request = http.MultipartRequest('POST', uri)
        ..headers["Content-Type"] = "application/json"
        ..headers["Authorization"] = "Bearer ${User.token}";
    }

    var multiPartFile =
        await http.MultipartFile.fromPath('file', pictureFile.path);
    request.files.add(multiPartFile);

    var response = await request.send();

    if (response.statusCode == 200) {
      String responseBody = await response.stream.bytesToString();
      var data = jsonDecode(responseBody);

      String imagePath = data['data'][0];

      return ApiReturnValue(value: imagePath);
    } else {
      return ApiReturnValue(message: 'uploading profile picture failes');
    }
  }
}
