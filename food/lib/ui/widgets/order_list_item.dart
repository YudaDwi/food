part of 'widgets.dart';

class OrderListItem extends StatelessWidget {
  final Transaction transaction;
  final double itemWidth;

  OrderListItem({@required this.transaction, this.itemWidth});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        //foto
        Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(right: 12),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              image: DecorationImage(
                  image: NetworkImage(transaction.food.picturePath),
                  fit: BoxFit.cover)),
        ),
        //text
        //biar gak tembus samping column di batasi pakai sizedbox
        SizedBox(
          //1.12 60*image - 12*margin - 110*rting star
          width: itemWidth - 60 - 12 - 110,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                transaction.food.name,
                style: blackFontStyle2,
                maxLines: 1,
                overflow: TextOverflow.clip,
              ),
              Text(
                '${transaction.quantity} item(s)' +
                    NumberFormat.currency(
                            symbol: 'IDR ', decimalDigits: 0, locale: 'id-ID')
                        .format(transaction.total),
                style: greyFontStyle.copyWith(fontSize: 13),
              )
            ],
          ),
        ),
        //rating star
        SizedBox(
          width: 110,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                convertDateTime(transaction.dateTime),
                style: greyFontStyle.copyWith(fontSize: 12),
              ),
              (transaction.status == TransactionStatus.cancelled)
                  ? Text(
                      'cancelled',
                      style: GoogleFonts.poppins(
                          color: 'd9435e'.toColor(), fontSize: 10),
                    )
                  : (transaction.status == TransactionStatus.pending)
                      ? Text(
                          'pending',
                          style: GoogleFonts.poppins(
                              color: 'd9435e'.toColor(), fontSize: 10),
                        )
                      : (transaction.status == TransactionStatus.on_delivery)
                          ? Text(
                              'on delivery',
                              style: GoogleFonts.poppins(
                                  color: '1abc9c'.toColor(), fontSize: 10),
                            )
                          : SizedBox()
            ],
          ),
        )
      ],
    );
  }

  //1.12 buat fungsi convert untuk tgl waktu bulan
  String convertDateTime(DateTime dateTime) {
    String month;

    switch (dateTime.month) {
      case 1:
        month = 'jan';
        break;
      case 2:
        month = 'feb';
        break;
      case 3:
        month = 'mar';
        break;
      case 4:
        month = 'apr';
        break;
      case 5:
        month = 'may';
        break;
      case 6:
        month = 'jun';
        break;
      case 7:
        month = 'jul';
        break;
      case 8:
        month = 'aug';
        break;
      case 9:
        month = 'sep';
        break;
      case 10:
        month = 'oct';
        break;
      case 11:
        month = 'nov';
        break;

      default:
        month = 'des';
    }
    return month + ' ${dateTime.day}, ${dateTime.hour}:${dateTime.minute}';
  }
}
