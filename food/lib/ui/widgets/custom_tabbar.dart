part of 'widgets.dart';

class CustomTabBar extends StatelessWidget {
  final int selectedIndex;
  final List<String> title;
  final Function(int) onTap;

  CustomTabBar({this.selectedIndex, @required this.title, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      //1.11 jarak atas sampai garis hitam
      height: 50,
      child: Stack(
        children: [
          //garis abu abu
          Container(
            margin: EdgeInsets.only(top: 48),
            height: 1,
            color: 'f2f2f2'.toColor(),
          ),
          //title
          Row(
            children: title
                .map((e) => Padding(
                      padding: EdgeInsets.only(left: defaultMargin),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          //untuk text dan garis hitam
                          GestureDetector(
                            onTap: () {
                              if (onTap != null) {
                                onTap(title.indexOf(e));
                              }
                              //1.11 akan mengembalikan ontap yg sesuai dgn title index yg dipilih
                            },
                            child: Text(
                              e,
                              style: (title.indexOf(e) == selectedIndex)
                                  ? blackFontStyle3.copyWith(
                                      fontWeight: FontWeight.w500)
                                  : greyFontStyle,
                            ),
                          ),
                          Container(
                              width: 40,
                              height: 3,
                              margin: EdgeInsets.only(top: 13),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(1.5),
                                  color: (title.indexOf(e) == selectedIndex)
                                      ? '020202'.toColor()
                                      : Colors.transparent))
                        ],
                      ),
                    ))
                .toList(),
          )
        ],
      ),
    );
  }
}
