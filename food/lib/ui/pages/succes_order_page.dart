part of 'pages.dart';

class SuccesOrderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: IllustrationPage(
        title: "you've made order",
        subtitle: 'just stay at home while we are\npreparing your best foods',
        picturePath: 'assets/bike.png',
        buttonTitle1: 'order other foods',
        buttonTap1: () {
          Get.offAll(MainPage());
        },
        buttonTap2: () {
          Get.off(MainPage(
            initialPage: 1,
            //2.6 maksudnya ternyata masuk ke status pembelian
          ));
        },
        buttonTitle2: 'view my order',
      ),
    );
  }
}
