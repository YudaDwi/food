part of 'pages.dart';

class GeneralPage extends StatelessWidget {
  final String title;
  final String subtitle;
  final Function onBackButtonPressed;
  final Widget child; //1.1 belum tau fungsi ini
  final Color backColor;

  GeneralPage(
      {this.title = 'title',
      this.subtitle = 'subtitle',
      this.onBackButtonPressed,
      this.child,
      this.backColor});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          color: Colors.white,
        ),
        //1.1 kalau backColor nya null maka diisi fafafc
        SafeArea(
            child: Container(
          color: backColor ?? Colors.white,
        )),
        SafeArea(
            child: ListView(
          children: [
            Column(
              children: [
                Container(
                  width: double.infinity,
                  height: 108,
                  color: Colors.white,
                  padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                  child: Row(
                    children: [
                      onBackButtonPressed != null
                          ? GestureDetector(
                              onTap: () {
                                if (onBackButtonPressed != null) {
                                  onBackButtonPressed();
                                }
                              },
                              child: Container(
                                width: 24,
                                height: 24,
                                margin: EdgeInsets.only(right: 26),
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/back_arrow.png'))),
                              ),
                            )
                          : SizedBox(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            title,
                            style: GoogleFonts.poppins(
                                fontSize: 22, fontWeight: FontWeight.w500),
                          ),
                          Text(subtitle,
                              style: GoogleFonts.poppins(
                                  color: '8d92a3'.toColor(),
                                  fontWeight: FontWeight.w300))
                        ],
                      )
                    ],
                  ),
                ),
                //1.2 warna abu abu
                Container(
                    height: defaultMargin,
                    width: double.infinity,
                    color: 'fafafc'.toColor()),
                //1.1 kalau child null maka diisi sizedbox kosong
                child ?? SizedBox()
              ],
            )
          ],
        ))
      ],
    ));
  }
}
