part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Sign In',
      subtitle: 'Find your best ever meal',
      child: Column(
        children: [
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 26, defaultMargin, 6),
            child: Text(
              'Email address',
              style: blackFontStyle2,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Colors.black),
            ),
            child: TextField(
                controller: emailController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: 'type your email address')),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.fromLTRB(defaultMargin, 16, defaultMargin, 6),
            child: Text(
              'Password',
              style: blackFontStyle2,
            ),
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: defaultMargin),
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              border: Border.all(color: Colors.black),
            ),
            child: TextField(
                controller: passwordController,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    hintStyle: greyFontStyle,
                    hintText: 'type your password')),
          ),
          //1.2 tombol
          Container(
            width: double.infinity,
            height: 45,
            margin: EdgeInsets.only(top: 45),
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            //1.2 kalau isloading itu true maka diisi spinkit kalau tidk
            child: isLoading
                ? loadingIndicator
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        primary: mainColor),
                    onPressed: () async {
                      //2.3 isloading diisi true karena memng sudah benar
                      setState(() {
                        isLoading = true;
                      });

                      //2.3 sepertinya sama dgn bloc
                      await context.bloc<UserCubit>().signIn(
                          emailController.text, passwordController.text);

                      //2.3 mengambil state nya saat ini
                      UserState state = context.bloc<UserCubit>().state;

                      //dicek state saat ini userloaded maka berhasil
                      // ngeload makanan nya/list dan juga transaction nya:
                      if (state is UserLoaded) {
                        context.bloc<FoodCubit>().getFoods();
                        context.bloc<TransactionCubit>().getTransaction();
                        Get.to(MainPage());
                      } else {
                        Get.snackbar('', '',
                            backgroundColor: 'd9435e'.toColor(),
                            icon: Icon(
                              MdiIcons.closeCircleOutline,
                              color: Colors.white,
                            ),
                            titleText: Text(
                              'sign in failed',
                              style: GoogleFonts.poppins(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ),
                            messageText: Text(
                                (state as UserLoadingFailed).message,
                                style:
                                    GoogleFonts.poppins(color: Colors.white)));
                        setState(() {
                          isLoading = false;
                        });
                      }
                    },
                    child: Text(
                      'Sign in',
                      style: blackFontStyle3,
                    )),
          ),
          Container(
            width: double.infinity,
            height: 45,
            margin: EdgeInsets.only(top: 12),
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            //1.2 kalau isloading itu true maka diisi spinkit kalau tidk
            child: isLoading
                ? SpinKitFadingCircle(
                    size: 45,
                    color: mainColor,
                  )
                : ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        primary: greyColor),
                    onPressed: () {
                      Get.to(SignUpPage());
                    },
                    child: Text(
                      'create new account',
                      style: blackFontStyle3.copyWith(color: Colors.white),
                    )),
          ),
        ],
      ),
    );
  }
}
