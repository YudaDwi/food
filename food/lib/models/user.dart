part of 'models.dart';

class User extends Equatable {
  final int id;
  final String name;
  final String email;
  final String address;
  final String houseNumber;
  final String phoneNumber;
  final String city;
  final String picturePath;
  static String token;

  User(
      {this.id,
      this.name,
      this.email,
      this.address,
      this.houseNumber,
      this.phoneNumber,
      this.city,
      this.picturePath});

  //3.1 method copywith berfungsi untuk satu buah objek user menjadi satu buah objek user yg baru
  //tapi dgn properti yg bisa kita ubah ubah sesuai dgn parameter yg dimasukan
  //ini mirip dgn font.copywith lalu bisa diedit oleh kita dgn parameter yg sesuai dgn yg ada

  User copyWith(
          {int id,
          String name,
          String email,
          String address,
          String houseNumber,
          String phoneNumber,
          String city,
          String picturePath}) =>
      //lalu dia mengembalikan user yg baru
      //id yg baru akan diisi oleh id yg ada diparameter kalau dicek/null maka akan kembali diisi id yg lama

      User(
          id: id ?? this.id,
          name: name ?? this.name,
          email: email ?? this.email,
          address: address ?? this.address,
          houseNumber: houseNumber ?? this.houseNumber,
          phoneNumber: phoneNumber ?? this.phoneNumber,
          city: city ?? this.city,
          picturePath: picturePath ?? this.picturePath);

  @override
  List<Object> get props =>
      [id, name, email, address, houseNumber, phoneNumber, city, picturePath];
}

User mockUser = User(
    id: 1,
    name: 'yuda kun',
    email: 'yudadwi0@gmail.com',
    address: 'ngrupit jenangan ',
    houseNumber: '21',
    phoneNumber: '085204912761',
    city: 'ponorogo',
    picturePath:
        'https://res.cloudinary.com/dk0z4ums3/image/upload/v1594169572/attached_image/daftar-makanan-tinggi-garam-yang-perlu-diwaspadai.jpg');
