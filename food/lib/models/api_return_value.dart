part of 'models.dart';

class ApiReturnValue<T> {
  final T value; //2.1 kalau data nya berhasil maka akan mengembalikan value
  final String
      message; //kalau gagal maka akan mengembalikan pesan kesalahan lewat sini

  ApiReturnValue({this.value, this.message});
}
